#!/bin/bash
echo -n "BOOTSTRAP:"
#https://unix.stackexchange.com/questions/314133/vsftpd-500-oops-cannot-change-directory
echo -n "CHMOD:"
chmod 755 /home
echo -n "OPTIONS:"

URLOPTION=""
[[ -z "${URL}" ]] || URLOPTION="url=$URL,"
[[ "$PATHREQUESTSTYLE" = "true"  ]] && URLOPTION="${URLOPTION}use_path_request_style,"
[[ "$CURLDEBUG" = "true"  ]] && URLOPTION="${URLOPTION}curldbg,"
echo -n "CONFIGS:"

#new config
echo > /etc/vsftpd.conf


### ALPINE FAILS: 500  OOPS: tcp_wrappers is set to YES but no tcp wrapper support compiled in
###### if only  specific ip adresses are allowed
####[[ -z "${ALLOWEDIPS}" ]] || {
####echo vsftpd: ALL        >> /etc/hosts.deny
####echo vsftpd:$ALLOWEDIPS >> /etc/hosts.allow
####echo tcp_wrappers=YES >> /etc/vsftpd.conf
####echo -n ; } ;
echo 
[[ -z "${ALLOWEDIPS}" ]] || {
echo -n "IPTABLES:"
iptables -I INPUT -p tcp --dport 21 -j DROP
iptables -I INPUT -p tcp --dport 21 -m state --state NEW -j LOG
for allowip in $(echo ${ALLOWEDIPS}|sed 's/-/ /g;s/,/ /g;s/ /\n/g' | grep -v ^$);do
iptables -I INPUT -p tcp --dport 21 -s $allowip -j ACCEPT;done
  echo -n ; } ;
set -euo pipefail
set -o errexit

trap 'kill -SIGQUIT $PID' INT
echo -n "EXTIP:"
# VSFTPD PASV configuration
#PASV_ADDRESS=${PASV_ADDRESS:-$(timeout 1 wget -qO- http://169.254.169.254/latest/meta-data/public-ipv4 2>/dev/null ||:)}
PASV_ADDRESS=$(timeout 5 curl -4  -s https://universaltool.herokuapp.com/showip 2>/dev/null || timeout 5 curl -s -4 https://ifconfig.co|| timeout 5 curl -4 -s whatismyip.akamai.com)
PASV_MIN_PORT=${PASV_MIN_PORT:-21000}
PASV_MAX_PORT=${PASV_MAX_PORT:-21000}
echo -n "VSFTP:"

# VSFTPD Banner
FTPD_BANNER=${FTPD_BANNER:-S3FS FTP Server}

# FTP allowed commands
# full command list : https://blog.vigilcode.com/2011/08/configure-secure-ftp-with-vsftpd/
CMDS_ALLOWED=${CMDS_ALLOWED:-ABOR,ALLO,APPE,CCC,CDUP,CWD,DELE,EPSV,FEAT,HELP,LIST,LPSV,MKD,MLST,MODE,NLST,NOOP,OPTS,PASS,PASV,PBSZ,PORT,PWD,QUIT,REIN,REST,RETR,RMD,RNFR,RNTO,SITE,SIZE,STAT,STOR,STRU,SYST,TYPE,USER}
test -e /tmp/vsftpd_log_file || mkfifo /tmp/vsftpd_log_file

# Configure vsftpd
echo "anonymous_enable=NO
seccomp_sandbox=NO
local_enable=YES
write_enable=YES
xferlog_enable=YES
log_ftp_protocol=YES
nopriv_user=${FTPD_USER}
allow_writeable_chroot=YES
chroot_local_user=YES
user_config_dir=/etc/vsftpd_user_conf
delete_failed_uploads=YES
port_enable=YES
port_promiscuous=YES
pasv_promiscuous=YES
cmds_allowed=$CMDS_ALLOWED
ftpd_banner=$FTPD_BANNER
pasv_enable=YES
vsftpd_log_file=/tmp/vsftpd_log_file
pasv_addr_resolve=NO
pasv_min_port=$PASV_MIN_PORT
pasv_max_port=$PASV_MAX_PORT" >> /etc/vsftpd.conf
test -e /etc/vsftpd_user_conf || mkdir /etc/vsftpd_user_conf
echo "local_root=/home/${FTPD_USER}/s3fs" > /etc/vsftpd_user_conf/${FTPD_USER}

[ -n "$PASV_ADDRESS" ] && echo "pasv_address=$PASV_ADDRESS" >> /etc/vsftpd.conf
tail -f /tmp/vsftpd_log_file &


echo -n "SSL_CRT:"
# SSL certificate
SSL_CERT_C=${SSL_CERT_C:-US}
SSL_CERT_ST=${SSL_CERT_ST:-Nowhere}
SSL_CERT_L=${SSL_CERT_L:-Middleearth}
SSL_CERT_O=${SSL_CERT_O:-s3sftp}
SSL_CERT_OU=${SSL_CERT_OU:-Hosting}
SSL_CERT_CN=${SSL_CERT_CN:-mydomain.lan}

# Create SSL certificate
test -e /etc/ssl/private/vsftpd.pem || openssl req -x509 -nodes -days 365 -newkey rsa:4096 -subj "/C=${SSL_CERT_C}/ST=${SSL_CERT_ST}/L=${SSL_CERT_L}/O=${SSL_CERT_O}/OU=${SSL_CERT_OU}/CN=${SSL_CERT_CN}" -keyout /etc/ssl/private/vsftpd.pem -out /etc/ssl/private/vsftpd.pem 2>/dev/null && echo "
rsa_cert_file=/etc/ssl/private/vsftpd.pem
rsa_private_key_file=/etc/ssl/private/vsftpd.pem
ssl_enable=YES
allow_anon_ssl=YES
force_anon_data_ssl=NO
force_anon_logins_ssl=NO
force_local_data_ssl=NO
force_local_logins_ssl=NO
ssl_tlsv1=YES
ssl_sslv2=NO
ssl_sslv3=NO
#implicit_ssl=YES
listen_port=21
require_cert=NO
require_ssl_reuse=NO
ssl_ciphers=HIGH" >> /etc/vsftpd.conf

# Amazon S3 bucket
S3_ACL=${S3_ACL:-private}
S3_BUCKET=${S3_BUCKET:-s3bucket}

# Amazon credentials
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:-aws_access_key_id}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:-aws_secret_access_key}

# VSFTPD credentials
FTPD_USER=${FTPD_USER:-s3ftp}
FTPD_PASS=${FTPD_PASS:-s3ftp}

# Multi users
FTPD_USERS=${FTPD_USERS:-${FTPD_USER}::${FTPD_PASS}::${S3_BUCKET}::${AWS_ACCESS_KEY_ID}::${AWS_SECRET_ACCESS_KEY}}

echo -n "USERS:"

# For each user
echo "${FTPD_USERS}" |sed 's/ /\n/g' |while read line; do
  echo ${line//::/ } |while read ftpd_user ftpd_pass s3_bucket aws_access_key_id aws_secret_access_key; do
    echo -n "$ftpd_user"

    # Create FTP user
    test -e /home/${ftpd_user} || adduser -h /home/${ftpd_user} -s /sbin/nologin -D ${ftpd_user}
    echo "${ftpd_user}:${ftpd_pass:-$FTPD_PASS}" | chpasswd 2> /dev/null

    # Configure s3fs
    echo "${aws_access_key_id:-$AWS_ACCESS_KEY_ID}:${aws_secret_access_key:-$AWS_SECRET_ACCESS_KEY}" > /tmp/.passwd-s3fs
    chmod 0400 /tmp/.passwd-s3fs

    # Mount s3fs
    mkdir -p /home/${ftpd_user}/s3fs
    chown ${ftpd_user} /home/${ftpd_user}/s3fs
    chgrp ${ftpd_user} /home/${ftpd_user}/s3fs
    echo starting loop /usr/bin/s3fs ${s3_bucket:-$S3_BUCKET} /home/${ftpd_user}/s3fs -f -o ${URLOPTION}nosuid,nonempty,nodev,allow_other,complement_stat,mp_umask=027,uid=$(id -u ${ftpd_user}),gid=$(id -g ${ftpd_user}),passwd_file=/tmp/.passwd-s3fs,default_acl=${S3_ACL},retries=5
    ( while (true);do /usr/bin/s3fs ${s3_bucket:-$S3_BUCKET} /home/${ftpd_user}/s3fs -f -o ${URLOPTION}nosuid,nonempty,nodev,allow_other,complement_stat,mp_umask=027,uid=$(id -u ${ftpd_user}),gid=$(id -g ${ftpd_user}),passwd_file=/tmp/.passwd-s3fs,default_acl=${S3_ACL},retries=5 ; sleep 0.2;done) &

    # Exit docker if the s3 filesystem is not reachable anymore
    #( crontab -l && echo "* * * * * timeout 3 touch /home/${ftpd_user}/.test >/dev/null || kill -KILL -1" ) | crontab -

  done
done

echo -n "SSHFS:"

# Enable SFTP
echo "Protocol 2
HostKey /etc/ssh/ssh_host_ed25519_key
HostKey /etc/ssh/ssh_host_rsa_key
UseDNS no
PermitRootLogin no
X11Forwarding no
AllowTcpForwarding no
Subsystem sftp internal-sftp
ForceCommand internal-sftp -d %u
ChrootDirectory /home
Port 1022
" > /etc/ssh/sshd_config


# FTP sync client
FTP_SYNC=${FTP_SYNC:-0}
FTP_HOST=${FTP_HOST:-localhost}
DIR_REMOTE=${DIR_REMOTE:-/}
DIR_LOCAL=${DIR_LOCAL:-/home/$FTPD_USER}
echo -n "SYNC:"

# Sync remote FTP every hour (at random time to allow multiple dockers to run)
[ "$FTP_SYNC" != "0" ] \
  && MIN=$(awk 'BEGIN { srand(); printf("%d\n",rand()*60)  }') \
  && ( echo "$MIN * * * * /usr/local/bin/lftp-sync.sh $FTP_HOST $DIR_REMOTE $DIR_LOCAL/retour/\$(/bin/date +%Y/%m/%d) ^8.*$" ) | crontab -u ${FTPD_USER} - \
  && MIN=$(awk 'BEGIN { srand(); printf("%d\n",rand()*rand()*60)  }') \
  && ( crontab -u ${FTPD_USER} -l && echo "$MIN * * * * /usr/local/bin/lftp-sync.sh $FTP_HOST $DIR_REMOTE $DIR_LOCAL/facture ^INV.*$" ) | crontab -u ${FTPD_USER} - \
  && touch /var/log/lftp-sync.log \
  && chown ${FTPD_USER} /var/log/lftp-sync.log

# Launch crond
crond -L /var/log/crond.log

echo " Launch sshd && vsftpd"

echo "starting vsftp"
[ $# -eq 0 ] && /usr/sbin/sshd -e &
[ $# -eq 0 ] && /usr/sbin/vsftpd || exec "$@" 
PID=$! 

